import numpy as np
import re
import subprocess

n_rep = 10

class Stat(object):
    def __init__(self,data):
        self._data = data

    def avg(self):
        return np.average(self._data)

    def min(self):
        return np.min(self._data)

    def max(self):
        return np.max(self._data)

    def stddev(self):
        return np.std(self._data)

    def __str__(self):
        s = ''
        s += ('%6.3f' % self.avg())+' +/- '
        s += ('%6.3f' % self.stddev())
        return s

def run(blocksize,nblocks,aligned_malloc):
    aligned_malloc_i = (1 if aligned_malloc else 0)
    cmd = './driver.x '+' '+str(blocksize)+' '+str(nblocks)+' '+str(aligned_malloc_i)
    output = subprocess.check_output(cmd,shell=True)
    for line in output.split('\n'):
        m = re.match(' *double GFLOP\/s *= *([0-9]+\.[0-9]+) *',line)
        if m:
            flops_double = float(m.group(1))
        m = re.match(' *Vec8d GFLOP\/s *= *([0-9]+\.[0-9]+) *',line)
        if m:
            flops_vec8d = float(m.group(1))
    return flops_double, flops_vec8d

if (__name__ == '__main__'):
    blocksize_ref = 22
    nblocks_ref = 100000
    for aligned_malloc in (True, False):
        print
        print 'aligned_malloc = '+str(aligned_malloc)
        print
        print 'blocksize    GFLOP/s [double]    GFLOP/s [Vec8d]'
        for blocksize in range(4,32):
            nblocks = int(nblocks_ref*(blocksize_ref/(1.*blocksize))**2)
            flops_double = []
            flops_vec8d = []
            for i in range(n_rep):
                tmp_flops_double, tmp_flops_vec8d = run(blocksize,nblocks,aligned_malloc)
                flops_double.append(tmp_flops_double)
                flops_vec8d.append(tmp_flops_vec8d)
            
            print ('%4d' % blocksize)+'   '+str(Stat(flops_double))+'    '+str(Stat(flops_vec8d))
