#include <string>
#include <vectorclass.h>

class LinAlg {
public:
  LinAlg(const int n_) : n(n_), label("abstract") {}

  virtual void matvec(double* A, double* x, double* y) = 0;

  unsigned int nflops() {
    return 8*2*n*n;
  }
  
  const std::string get_label() const {
    return label;
  }

protected:
  LinAlg(const int n_, const std::string label_) : n(n_), label(label_) {}
  const int n;
  const std::string label;
};

class LinAlg_double : public LinAlg {
public:
  LinAlg_double(const int n_) : LinAlg(n_,"double") {}

  virtual void matvec(double* A, double* x, double* y) {
    for (int i=0;i<n;i++) {      
      y[8*i  ] = 0.0;
      y[8*i+1] = 0.0;
      y[8*i+2] = 0.0;
      y[8*i+3] = 0.0;
      y[8*i+4] = 0.0;
      y[8*i+5] = 0.0;
      y[8*i+6] = 0.0;
      y[8*i+7] = 0.0;
      for (int j=0;j<n;++j) {
	double tmp_A = A[n*i+j];
	y[8*i  ] += tmp_A*x[8*j  ];
	y[8*i+1] += tmp_A*x[8*j+1];
	y[8*i+2] += tmp_A*x[8*j+2];
	y[8*i+3] += tmp_A*x[8*j+3];
	y[8*i+4] += tmp_A*x[8*j+4];
	y[8*i+5] += tmp_A*x[8*j+5];
	y[8*i+6] += tmp_A*x[8*j+6];
	y[8*i+7] += tmp_A*x[8*j+7];
      }
    }
  }
};


class LinAlg_Vec8d : public LinAlg {
public:
  LinAlg_Vec8d(int n_) : LinAlg(n_,"Vec8d") {
    y_zero = (double*) malloc(8*sizeof(double));
    x_data = (Vec8d*) malloc(n*sizeof(Vec8d));
    y_data = (Vec8d*) malloc(n*sizeof(Vec8d));
  }

  ~LinAlg_Vec8d() {
    free(y_zero);
    free(x_data);
    free(y_data);
  }

  virtual void matvec(double* A, double* x, double* y) {
    // Load into vector storage
    for (int i=0;i<n;i++) {
      x_data[i].load(x+8*i);
      y_data[i].load(y_zero);
    }
    for (int i=0;i<n;i++) {      
      for (int j=0;j<n;++j) {
	y_data[i] += A[n*i+j]*x_data[j];
      }
    }
    // Copy back from vector storage
    for (int i=0;i<n;i++) {
      y_data[i].store(y+8*i);
    }
  }

private:
  double* y_zero;
  Vec8d* x_data;
  Vec8d* y_data;
};
