#include <stdlib.h>

class Storage {
public:
  Storage(const int blocksize_,
	  const int nblocks_,
	  const bool aligned_malloc_) : blocksize(blocksize_),
					nblocks(nblocks_),
					aligned_malloc(aligned_malloc_) {
    if (aligned_malloc) {
      data = (double*) _mm_malloc(8*blocksize*nblocks*sizeof(double),512);
    } else {
      data = (double*) malloc(8*blocksize*nblocks*sizeof(double));
    }
  }
  
  ~Storage() {
    if (aligned_malloc) {
      _mm_free(data);
    } else {
      free(data);
    }
  }

  double* operator[](int i) {
    return data+8*blocksize*i;
  }

private:
  const int blocksize;
  const int nblocks;
  const bool aligned_malloc;
  double* data;
};
