#include <iostream>
#include <vector>
#include <chrono>
#include <vectorclass.h>
#include "storage.hh"
#include "dense_matvec.hh"

int main(int argc, char* argv[]) {

  if (argc != 4) {
    std::cout << "Usage: " << argv[0] << " BLOCKSIZE NBLOCKS ALIGNEDMALLOC" << std::endl;
    exit(0);
  }
  const unsigned int random_seed = 91247507;
  int blocksize = atoi(argv[1]);
  int nblocks = atoi(argv[2]);
  bool aligned_malloc = atoi(argv[3]);
  std::cout << "blocksize        = "  << blocksize << std::endl;
  std::cout << "nblocks          = "  << nblocks << std::endl;
  std::cout << "aligned_malloc   = "  << aligned_malloc << std::endl;

  Storage x(blocksize,nblocks,aligned_malloc);
  Storage y(blocksize,nblocks,aligned_malloc);

  double* A = (double*) malloc(blocksize*blocksize*sizeof(double));

  srand(random_seed);
  for (int i=0;i<blocksize*blocksize;++i) A[i] = rand()/(1.0*RAND_MAX);

  std::vector<LinAlg*> la_imp;
  la_imp.push_back(new LinAlg_double(blocksize));
  la_imp.push_back(new LinAlg_Vec8d(blocksize));

  // Loop over all implementations
  for (auto item=la_imp.begin();item!=la_imp.end();++item) {
    std::cout << "*** Implementation : " << (*item)->get_label() << std::endl;
    // Initialise the vector x with random numbers
    srand(random_seed);
    for (int i=0;i<nblocks;++i) {
      for (int j=0;j<8*blocksize;++j) {
	x[i][j] = rand()/(1.0*RAND_MAX);
      }
    }
    auto t_start = std::chrono::system_clock::now();
    for (int i=0;i<nblocks;++i) {
      (*item)->matvec(A,x[i],y[i]);
    }  
    auto t_finish = std::chrono::system_clock::now();
    auto elapsed = 
      std::chrono::duration_cast<std::chrono::milliseconds>(t_finish - t_start);
    double t_elapsed = elapsed.count();
    std::cout << (*item)->get_label() << " time    = " << t_elapsed << " ms" << std::endl;
    double flops = nblocks*(*item)->nflops();
    std::cout << (*item)->get_label() << " GFLOP/s = " << 1.E-9*flops/(1.E-3*t_elapsed) << std::endl;
    std::cout << std::endl;
  }

  free(A);
}
