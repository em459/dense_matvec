CC=icpc

# Location of vectorclass library
VECTORCLASS_LIB_DIR=${HOME}/scratch/library/vectorclass

CFLAGS=-march=skylake-avx512 -DMAX_VECTOR_SIZE=512 -std=c++11 -I$(VECTORCLASS_LIB_DIR)
LFLAGS=

# Object files
OBJS=driver.o storage.o dense_matvec.o

all: driver.x

%.o: %.cc
	$(CC) $(CFLAGS) -c -o $@ $<

%.o: %.cc %.hh
	$(CC) $(CFLAGS) -c -o $@ $<

driver.x: $(OBJS)
	$(CC) $(LFLAGS) -o driver.x $(OBJS) 

.phony: clean
clean:
	rm -f *.o driver.x